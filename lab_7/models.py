from django.db import models

# Create your models here.
class Friend(models.Model):
    friend_name = models.CharField(max_length=400)
    npm = models.CharField(max_length=250, unique=True)
    
    address = models.CharField(max_length=400, null=True)
    postal_code = models.CharField(max_length=400, null=True)
    birth_place = models.CharField(max_length=400, null=True)
    birth_date = models.CharField(max_length=400, null=True)
    
    generation = models.CharField(max_length=400, null=True)
    study_field = models.CharField(max_length=400, null=True)
    study_program = models.CharField(max_length=400, null=True)
    added_at = models.DateField(auto_now_add=True)
