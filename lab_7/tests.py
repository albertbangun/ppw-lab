from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.db.models.manager import Manager
from unittest.mock import patch
from .views import (
	index, add_friend, validate_npm, delete_friend, 
    friend_list, friend_detail, get_friend_list
)
from .models import Friend
from .api_csui_helper.csui_helper import CSUIhelper

# Create your tests here.
class Lab7UnitTest(TestCase):
	def test_lab_7_url_is_exist(self):
		response = Client().get('/lab-7/')
		self.assertEqual(response.status_code, 200)

	def test_lab7_using_index_func(self):
		found = resolve('/lab-7/')
		self.assertEqual(found.func, index)

	def test_friend_list_url_is_exist(self):
		response = Client().get('/lab-7/friend-list/')
		self.assertEqual(response.status_code, 200)

	def test_get_friend_list_data_url_is_exist(self):
		response = Client().get('/lab-7/get-friend-list/')
		self.assertEqual(response.status_code, 200)
	
	def test_friend_description_url_is_exist(self):
		friend = Friend.objects.create(friend_name="Setnov", npm="12345")
		response = Client().post('/lab-7/friend-detail/' +str(friend.id)+ '/')
		self.assertEqual(response.status_code, 200)
	
	def test_auth_param_dict(self):
		csui_helper = CSUIhelper()
		auth_param = csui_helper.instance.get_auth_param_dict()
		self.assertEqual(auth_param['client_id'], csui_helper.instance.get_auth_param_dict()['client_id'])
	
	def test_add_basic_friend(self):
		response_post = Client().post(
			'/lab-7/add-friend/', 
			{'name':"Setnov", 'npm':"wkwkwk"}
		)
		self.assertEqual(response_post.status_code, 200)
		
	def test_add_advanced_friend(self):
		response_post = Client().post(
			'/lab-7/add-friend/', 
			{'name':"Setnov",
			'npm':"wkwkwk",
			'alamat':"gedung dpr",
			'kode_pos':"12345",
			'kota_lahir':"istana",
			'tgl_lahir':'120998',
			'angkatan':'1998',
			'nm_org':'IlmuKomedi',
			'nm_prg':'wkwkwk2',}
		)
		self.assertEqual(response_post.status_code, 200)

	def test_delete_friend(self):
		friend = Friend.objects.create(friend_name="Polisi", npm="wkwkwkwk")
		response = Client().get('/lab-7/delete-friend/{}/'.format(friend.id))
		self.assertEqual(response.status_code, 302)

		counting_all_friends = Friend.objects.all().count()
		self.assertEqual(counting_all_friends, 0)
		
	def test_invalid_sso_raise_exception(self):
		username = 'ini user'
		password = 'ini password'
		csui_helper = CSUIhelper()
		with self.assertRaises(Exception) as context:
			csui_helper.instance.get_access_token(username, password)
		self.assertIn("ini user", str(context.exception))
	
	def test_validate_npm(self):
		response = self.client.post('/lab-7/validate-npm/')
		html_response = response.content.decode('utf8')
		self.assertEqual(response.status_code, 200)
		self.assertJSONEqual(html_response, {'is_taken':False})
	
